# init script for Vagrant VMs

# update OS
dnf update -y

# install ansible
dnf install -y ansible

# désactive SELinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config

# user droit root
useradd -m -G vagrant saru
echo "saru ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/saru
mkdir /home/saru/.ssh
chmod 700 /home/saru/.ssh
cat /home/vagrant/id_ed25519.pub >> /home/saru/.ssh/authorized_keys
chmod 600 /home/saru/.ssh/authorized_keys
chown -R saru.saru /home/saru/.ssh
rm -f /home/vagrant/id_ed25519.pub