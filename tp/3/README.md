# TP3 : First boot conf

Dans les premiers TPs, on s'est contentés de configurer avec un script shell les VMs losqu'elles s'allument, ainsi qu'une gestion assez manuelle des clés SSH.

➜ Dans ce troisième TP on va s'intéresser à cette brique manquante concernant notre gestion de conf : **une gestion avancée du premier boot de nos machines**, afin de se passer de ce script shell.

C'est une problématique *primordiale* dans la gestion d'infrastructure qui veut s'orienter vers un parc virtuel entièrement dynamique et automatisé, au sein duquel il est aisé et rapide de créer de nouvelles machines.

➜ On va s'intéresser principalement à deux technos dans ce TP, une partie est dédiée à chacune :

- **PXE**
  - utilisé en environnement traditionnel
  - auto-hébergé, avec des hyperviseurs partout
  - PXE permet à une VM sans OS de récupérer un `.iso` à travers le réseau, et de lancer une installation
  - on va le coupler à *Kickstart*, qui permet d'automatiser ce processus d'installation
- **`cloud-init`**
  - utilisé en environnement cloud (OpenStack, OpenNebula, AWS, Azure, etc.)
  - dans notre cas, c'est l'environnement Vagrant
  - permet de déposer sur une machine de la configuration lors de son tout premier boot

# 0. Setup

➜ **Créez un nouveau répertoire de travail dans votre dépôt git `tp3/`**

# I. Traditional self-hosted

## 0. Intro

Dans cette première partie, on va se placer dans une situation qu'on trouve encore dans beaucoup d'environnements : **le boot à travers le réseau.**

Pour cela, on va faire appel à ***PXE*** qui est le protocole de référence.

> *PXE* n'est pas spécifique à des OS GNU/Linux.

PXE repose lui-même sur d'autres protocoles très utilisés : DHCP et aussi souvent TFTP et HTTP afin d'interagir avec les machines qui ont besoin d'une installation réseau.

## 1. PXE server

> Utilisez une VM Rocky Linux 9 pour dérouler cette partie. Créée à la main ou *via* Vagrant, peu importe. On va aller à l'essentiel et dérouler cette install à la main. Peut-être que je vous demanderai d'écrire un *role* Ansible pour ça plus tard, qui sait ? :D

➜ Je vais pas réinventer la roue pour cette section, je vous file une bonne ressource, que j'ai encore testée récemment : https://www.server-world.info/en/note?os=CentOS_Stream_9&p=pxe&f=1

Ne faites que les parties 1, 2 et 3 sur le lien, on testera peut-être du Kickstart plus tard.

# II. Environnement Cloud

## 0. Intro

➜ **On parle *d'environnement Cloud*** lorsqu'on utilise des plateformes auto-hébergées comme *OpenStack* et *OpenNebula*, ou hébergées en ligne comme *AWS* ou *Azure*.

Ce sont des environnements qui permettent de déployer des ressources virtuelles (comme des VMs), avec des fonctionnalités avancées qui dépassent celles d'un simple hyperviseur.

> Dans notre cas, on utilise le beau, le fort, le bien-aimé Vagrant :D Il va nous permettre d'appréhender tout ça, sans avoir besoin de faire appel à un autre système que votre poste.

➜ Dans ce genre d'environnement, **il est récurrent d'instancier les VMs à partir d'un disque que l'on clone**, plutôt que de dérouler une installation classique à chaque lancement d'une nouvelle machine (que cette installation soit automatisée ou non).

Ceci dans une optique de rapidité lors du démarrage d'une nouvelle machine.

**Une fois la VM créée, elle démarre l'OS préinstallé sur le disque cloné.** Simple. Basique.

➜ **Un service appelé `cloud-init` va s'exécuter au premier lancement de la machine.**

> Je ne vous apprends rien, on utilise habituellement des fichiers d'image disque (`.iso`) pour installer un OS. On trouve désormais pour les OS les plus utilisés une variante de l'image de base qui est appelée "image *cloud*". Des petits ajustements ont été effecués par rapport à l'image classique : l'OS est déjà installé et c'est seulement le disque qui est livré, prêt à être cloné (pas un `.iso` donc). De plus, le service `cloud-init` est déjà installé.

---

➜ **`cloud-init` est une techno utilisée pour autoconfigurer une machine lors de son premier boot**

Généralement, on va l'utiliser pour gérer :

- des users
- des clés SSH (dépôt de clé publique sur des users spécifiques)
- l'installation de certains paquets
- démarrage et conf de certains services élémentaires

---

➜ **Dans notre cas, on va monter le lab suivant :**

- utilisation d'un `.iso` de Rocky Linux type "cloud"
  - entre autres, il est configuré pour chercher une conf `cloud-init` lors du premier boot
- un premier boot manuel pour jouer avec l'image de type cloud et `cloud-init`
- préparation d'une box Vagrant
  - elle permettra d'instancier X VMs autoconfigurées avec `cloud-init`
  - elle se base sur notre précédente VM
  - on y aura ajouté la config nécessaire pour être utilisée par Vagrant
  - `cloud-init` est réinitialisé (pour qu'il se redéclenche quand on instanciera la box avec Vagrant)
- faire joujou avec notre nouvelle box Vagrant
  - instancier X machines
  - préparer pour une utilisation immédiate de Ansible, dès le premier boot, sans opération manuelle

## 1. Une première VM cloud-init

> *Vous trouverez sur [la page officielle des téléchargements Rocky Linux](https://rockylinux.org/fr/alternative-images) une image Rocky 9 "Generic Cloud/Openstack". Il existe une version ARM alors aucun pb pour les machines Apple avec proc ARM.*

**Le flow de cette partie va être le suivant** :

- récupérer l'image de "Generic Cloud" de Rocky si ce n'est pas déjà fait
- préparer un `.iso` qui contient notre conf `cloud-init`
- allumer la machine et constater les changements

> Sorry sorry mais les étapes sont données avec des commandes GNU/Linux. Basculez vers un OS de ce type est fortement recommandé (sur votre PC, ou dans une machine virutelle). Ca doit être gérable sans trop d'adaptation avec un Mac :)

Let's go :)

---

➜ **Convertir le fichier de disque** dans un format utilisable avec nos outils

- on a récupéré un fichier `.qcow2`
- pour être utilisable nativement par VirtualBox par exemple, il faut le convertir en `.vdi`
- si vous utilisez un autre hyperviseur, je vous laisserai adapter la commande

> On parle pas ici de changer bêtement une extension. Le format du fichier est complètement différent.

```bash
$ qemu-img  convert -f qcow2 -O vdi Rocky-9-GenericCloud-Base.latest.x86_64.qcow2 rocky9.vdi
```

➜ **La suite d'étape pour préparer le `.iso` :**

- préparer un fichier `user-data` avec le contenu suivant :

```yml
#cloud-config
users:
  - name: it4
    gecos: Super adminsys
    primary_group: it4
    groups: wheel
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$XURsDlAF20pG3D3W$8a9/8WMPYRpGJdCujP/WiLSiojYfObsxfLpYokoISUDO0LqkaNd6bp8tnqow29aNl1oXvgU4CNK.1q76WNfAs/
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINO/JQ3AtA3k8iXJWlkdUYZHDh215OKyLR0vauzD7uaB
```

> Adaptez évidemment avec les infos de votre choix, notamment : le nom du user, nom du groupe, son password, et la clé publique déposée.

- préparer un fichier `meta-data` avec le contenu suivant :

```yml
instance-id: first-cloud-init
local-hostname: tp3.gestion.conf
```

- générer le fichier `.iso` à partir des deux fichiers précédents

```bash
$ genisoimage -output cloud-init.iso -volid cloudinitdata -joliet -r meta-data user-data
```

➜ **Création de la VM**

- créer une VM manuellement dans VirtualBox (ou l'yperviseur de votre choix)
- donnez-lui en disque primaire le disque
- ajoutez comme sous la forme d'un CD-ROM notre `.iso` généré précédemment
- démarrez la VM et constatez que votre conf `cloud-init` a été appliquée

## 2. Avec Vagrant

Ici on va :

- réutiliser la VM de la partie précédente
- effectuer de la conf dans la VM pour
  - une utilisation ultérieure de `cloud-init`
  - préparer la machine à être utilisée *via* Vagrant
- importer la VM sous forme d'une *box* Vagrant
- tester l'instanciation de cette nouvelle *box*

> Vagrant est en mesure de passer une configuration `cloud-init` à une VM lorsqu'elle démarre. C'est une *feature* expérimentale, mais ça fait plusieurs années déjà, ça marche plutôt bien. Si vous avez un soucis avec elle, on fera sans. Référez-vous là encore à la [doc officielle](https://developer.hashicorp.com/vagrant/docs/experimental) pour indiquer à Vagrant de passer en mode expérimental.

➜ **Je vous laisse un peu autonomes pour la conf à réaliser. Le nécessaire c'est :**

- ajouter la conf demandée par Vagrant
  - [la doc officielle](https://developer.hashicorp.com/vagrant/docs/boxes/base) est très bien et claire à ce sujet
  - **n'installez pas** les *VirtualBox Guest Additions* dans la VM
    - c'est long, parfois fastidieux, et surtout assez inutile dans notre cas
- utilisez une commande `cloud-init` pour réinitialiser son état
  - ainsi, on pourra de nouveau passer une conf `cloud-init` lorsqu'on allumera des VMs avec Vagrant
- désactivez SELinux
- mettez à jour `cloud-init` avec la suite de commandes suivantes :

```bash
# Update cloud-init version
$ dnf -y copr enable @cloud-init/el-testing 
$ dnf update -y
$ dnf install -y cloud-init
```

➜ **On va transformer cette machine en *box* Vagrant** :

```bash
# Exporte la VM existante en un fichier .box
$ vagrant package --base <VM_NAME> --output rocky8-cloud.box

# Importe le .box dans les box disponibles pour Vagrant
$ vagrant box add rocky8-cloud.box --name rocky8-cloud
```

➜ **Préparer l'environnement Vagrant**

```bash
# Créer un nouveau répertoire de travail
$ mkdir tp3/vagrant
$ cd tp3/vagrant

# Initialiser un nouveau Vagrantfile
$ vagrant init rocky8-cloud

# Editer le Vagrantfile pour qu'il ressemble à :
$ cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "rocky8-cloud"
  # On désactive la synchro automatique du dossier courant, pour que ça marche, il faut les Guest Additions
  config.vm.synced_folder '.', '/vagrant', disabled: true

  # Ajout de la conf cloud-init
  config.vm.cloud_init do |cloud_init|
    cloud_init.content_type = "text/cloud-config"
    cloud_init.path = 'user-data'
  end
end
```

➜ **Préparer la conf `cloud-init`**

- toujours dans le dossier `vagrant/`
- créer un fichier `user-data` :

```yml
#cloud-config
users:
  - name: test
    gecos: deployed by cloud-init
    primary_group: test
    groups: wheel
    shell: /bin/bash
    lock_passwd: false
    passwd: $6$XURsDlAF20pG3C3W$8a9/8WMP7yuGGqCujP/WiLSEYjYfObUiuLpYokoISUDO0LqkaNd6bp8tnqow29aNl1oXvgU4CNK.1q76WNfAs/
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMO/JQ3AtA3k8iXJWlkd8zAHDh215OKyLR0vauzD7BgA
```

➜ **Tester d'instancier une VM**

- avec un `vagrant up`

## 3. Préparation Ansible

Enfin, dernière partie atour de `cloud-init` : s'en servir pour faire quelque chose d'utile et de très récurrent : préparer la machine à être contrôlée par Ansible.

> A noter que nous avions déjà fait ça au TP1 et TP2. Ou pas. On s'était contentés de réutiliser la clé SSH "insecure" de Vagrant, ou de déposer manuellement vos clés dans les VMs créées. Ici on parle d'une automatisation totale, qui fonctionne dans n'importe quel environnement de virtu, y compris les environnements Cloud (OpenStack, OpenNebula, AWS, GCP, etc.).

➜ **Ajuster la conf `cloud-init`**

- Ansible doit être installé
- un user qui porte votre nom/pseudo doit être déployé
  - il doit pouvoir accéder aux droits `sudo` (`NOPASSWD` pour pas se prendre la tête pendant les TPs)
  - vous devez poser votre clé SSH publique sur ce user

➜ **Créez un nouveau répertoire de travail tp3/ansible/**

- créer un nouveau dépôt Ansible (`inventories/`, `roles/`, `playbooks/`, `ansible.cfg`)
  - n'hésitez pas à récupérer le contenu d'un dépôt précédent (TP2)
- déployez une configuration sur la machine créée à l'aide de Vagrant

# Bilan

➜ **On obtient une chaîne de provisioning complètement automatisée, avec de la gestion de conf**

- définition et lancement de VM
- config initiale de chaque VM
- config additionnelle et vie de la VM

➜ **Connaissances sur des environnements différents**

- environnements Cloud
  - Vagrant + `cloud-init`
- environnements plus traditionnels
  - PXE + Kickstart
- quoiqu'il arrive, c'est ensuite Ansible qui prend le relais
