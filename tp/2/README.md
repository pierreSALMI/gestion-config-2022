# TP2 : Ansible et structure de dépôt

Le but de ce TP est d'approfondir l'utilisation d'Ansible et d'appréhender un peu plus à quoi peut ressembler un dépôt qui gère un grand parc de machines.

# Sommaire

- [TP2 : Ansible et structure de dépôt](#tp2--ansible-et-structure-de-dépôt)
- [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Structurer le dépôt Ansible](#i-structurer-le-dépôt-ansible)
  - [1. Structure du dépôt : inventaires](#1-structure-du-dépôt--inventaires)
  - [2. Structure du dépôt : rôles](#2-structure-du-dépôt--rôles)
  - [3. Structure du dépôt : variables d'inventaire](#3-structure-du-dépôt--variables-dinventaire)
  - [4. Structure du dépôt : rôle avancé](#4-structure-du-dépôt--rôle-avancé)
  - [5. Gérer la suppression](#5-gérer-la-suppression)
- [III. Affinons cette config](#iii-affinons-cette-config)
  - [1. NGINX](#1-nginx)
  - [2. Common](#2-common)
  - [3. Dynamic loadbalancer](#3-dynamic-loadbalancer)

# 0. Setup

➜ **Créez un nouveau répertoire de travail dans votre dépôt git `tp2/`**

➜ Comme dans le premier TP, **créez un sous-répertoire `tp2/vagrant/`**

- récupérez votre `Vagrantfile` du TP1 comme base
- on utilisera le réseau `10.2.1.0/24` pour ce TP2
- créez deux machines
  - `node1.tp2.lab`
    - porte l'IP `10.2.1.11`
    - porte le hostname `node1.tp2.lab` (Vagrant peut configurer le hostname de la machine)
  - `node2.tp2.lab`
    - porte l'IP `10.2.1.12`
    - porte le hostname `node2.tp2.lab`

> Le pattern pour les IPs c'est `10.tp_number.network_number.0/24` avec `network_number` qui s'incrémentera si on fait un TP avec plusieurs réseaux.

➜ **Modifiez le fichier `hosts` de votre machine**

- pour que les noms `node1.tp2.lab` et `node2.tp2.lab` pointent vers leurs IP respectives
- on pourra ainsi joindre les machines par IP, de façon plus élégante, et plus proche du monde réel
- ce sera nécessaire pour la suite

➜ **Regénérez-vous un fichier de conf SSH**

```bash
$ vagrant ssh-config > .ssh-config
```

➜ **créez un autre répertoire `tp2/ansible/`**

- créez un fichier d'inventaire `hosts.ini` comme au TP1
- récupérez aussi le `ansible.cfg`
- modifiez les IPs à l'intérieur et créez pour le moment un unique groupe `tp2` dans lequel sont définis les deux machines
- **déclarez-les machines avec leurs noms, pas leurs IPs**, c'est important pour la suite

> Dans un fichier d'inventaire, il est possible de déclarer une même IP (ou nom de domaine) dans plusieurs groupes.

# I. Structurer le dépôt Ansible

## 1. Structure du dépôt : inventaires

➜ **Dans votre répertoire de travail Ansible...**

- créez un répertoire `inventories/`
- créez un répertoire `inventories/production/`
- déplacez le fichier `hosts.ini` dans `inventories/production/hosts.ini`
- assurez vous que pouvez toujours déployer correctement avec une commande `ansible-playbook`

## 2. Structure du dépôt : rôles

➜ **Modification du fichier de config Ansible**

- dans le répertoire de travail, modifiez le fichier `ansible.cfg` et ajoutez :

```ini
[defaults]
roles_path = ./roles
```

➜ **Dans votre répertoire de travail Ansible...**

- créez un répertoire `roles/`
  - il hébergera tous nos *rôles* pour ce TP
- créez un répertoire `roles/common/`
- créez un répertoire `roles/common/tasks/`
- créez un fichier `roles/common/tasks/main.yml` avec le contenu suivant :

```yml
- name: Install common packages
  import_tasks: packages.yml
```

> Le `main.yml` va inclure tous les autres fichiers, dans un ordre cohérent, et parfois avec des conditions.

- créez un fichier `roles/common/tasks/packages.yml` :
  - on va en profiter pour manipuler des variables Ansible

```yml
- name: Install common packages
  ansible.builtin.package:
    name: "{{ item }}"
    state: present
  with_items: "{{ common_packages }}" # ceci permet de boucler sur la liste common_packages
```

- créez un répertoire `roles/common/defaults/`
- créez un fichier `roles/common/defaults/main.yml` :

```yml
common_packages:
  - vim
  - git
```

- créez un fichier `playbooks/main.yml`

```yml
- hosts: tp2
  roles:
    - common
```

➜ **Testez d'appliquer ce playbook avec une commande `ansible-playbook`**

## 3. Structure du dépôt : variables d'inventaire

Afin de garder la complexité d'un dépôt Ansible sous contrôle, il est récurrent d'user et abuser de l'utilisation des variables.

Il est possible dans un dépôt Ansible de déclarer à plusieurs endroits : on a déjà vu le répertoire `defaults/` à l'intérieur d'un *rôle* (comme notre `roles/common/defaults/`) que l'on a créé juste avant. Ce répertoire est utile pour déclarer des variables spécifiques au *rôle*.

Qu'en est-il, dans notre cas présent, si l'on souhaite installer un paquet sur une seule machine, mais qui est considéré comme un paquet de "base" ? On aimerait l'ajouter dans la liste dans `roles/common/defaults/main.yml` mais ce serait moche d'avoir une condition sur le nom de la machine à cet endroit (un rôle doit être générique).

**Pour cela on utilise les `host_vars`.**

➜ **Dans votre répertoire de travail Ansible...**

- créez un répertoire `inventories/production/host_vars/`
- créez un fichier `inventories/production/host_vars/node1.tp2.lab.yml` :

```yml
common_packages:
  - vim
  - git
  - rsync
```

> Le nommage du dossier `host_vars` est important : Ansible sait qu'il doit aller chercher dans ce dossier s'il existe. Idem pour le nom du fichier, qui doit être rigoureusement celui qui est déclaré dans l'inventaire.

➜ **Testez d'appliquer le playbook avec une commande `ansible-playbook`**

---

Il est aussi possible d'attribuer des variables à un groupe de machines définies dans l'inventaire. **On utilise pour ça les `group_vars`.**

➜ **Dans votre répertoire de travail Ansible...**

- créez un répertoire `inventories/production/group_vars/`
- créez un fichier `inventories/production/group_vars/tp2.yml` :

```yml
users:
  - alice
  - bob
  - eve
```

> Là encore le nommage du dossier est important, ainsi que du fichier. Pour celui du fichier, c'est le nom du groupe d'hôte déclaré dans l'inventaire.

➜ **Modifiez le fichier `roles/common/tasks/main.yml`** pour inclure un nouveau fichier  `roles/common/tasks/users.yml` :

- il doit utiliser cette variable `users` pour créer des utilisateurs
- réutilisez la syntaxe avec le `with_items`
- la variable `users` est accessible, du moment que vous déployez sur les machines qui sont dans le groupe `tp2`

➜ **Vérifiez la bonne exécution du playbook**

> Si vous ne la connaissiez pas, usez et abusez de la commande `tree` pour voir plus clair dans votre aborescence.

## 4. Structure du dépôt : rôle avancé

➜ **Créez un nouveau rôle `nginx`**

- créez le répertoire du rôle `roles/nginx/`
- créez un sous-répertoire `roles/nginx/tasks/` et un fichier `main.yml` à l'intérieur :

```yml
- name: Install NGINX
  import_tasks: install.yml

- name: Configure NGINX
  import_tasks: config.yml

- name: Deploy VirtualHosts
  import_tasks: vhosts.yml
```

➜ **Remplissez le fichier `roles/nginx/tasks/install.yml`**

- il doit installer le paquet NGINX
  - je vous laisse gérer :)

➜ **On va y ajouter quelques mécaniques : fichiers et templates :**

- créez un répertoire `roles/nginx/files/`
- créez un fichier `roles/nginx/files/nginx.conf`
  - récupérez un fichier `nginx.conf` par défaut (en faisant une install à la main par exemple)
- créez un répertoire `roles/nginx/templates/`
- créez un fichier `roles/nginx/templates/vhost.conf.j2` :
  - `.j2` c'pour Jinja2, c'est le nom du moteur de templating utilisé par Ansible

```nginx
server {
        listen {{ nginx_port }} ;
        server_name {{ nginx_servername }};

        location / {
            root {{ nginx_webroot }};
            index index.html;
        }
}
```

➜ **Remplissez le fichier `roles/nginx/tasks/config.yml`** :

```yml
- name : Main NGINX config file
  copy:
    src: nginx.conf # pas besoin de préciser de path, il sait qu'il doit chercher dans le dossier files/
    dest: /etc/nginx/nginx.conf
```

➜ **Quelques variables `roles/nginx/defaults/main.yml`** :

```yml
nginx_servername: test
nginx_port: 8080
nginx_webroot: /var/www/html/test
nginx_index_content: "<h1>teeeeeest</h1>"
```

➜ **Remplissez le fichier `roles/nginx/tasks/vhosts.yml`** :

```yml
- name: Create webroot
  file:
    path: "{{ nginx_webroot }}"
    state: directory

- name: Create index
  copy:
    dest: "{{ nginx_webroot }}/index.html"
    content: "{{ nginx_index_content }}"

- name: NGINX Virtual Host
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ nginx_servername }}.conf
```

➜ **Deploy !**

- ajoutez ce rôle `nginx` au playbook
- et déployez avec une commande `ansible-playbook`

![That feel](./pics/that_feel.jpg)

## 5. Gérer la suppression

Déployer et ajouter des trucs c'est bien beau, mais comment on fait pour gérer le changement ?

**Bah c'est la galère.** Et il faut de la rigueur.

➜ **Créez un fichier qui permet de supprimer des Virtual Hosts NGINX**

- renommez le fichier `roles/nginx/tasks/vhosts.yml` en `roles/nginx/tasks/add_vhosts.yml`
- créez le fichier `roles/nginx/tasks/remove_vhosts.yml` et remplissez le avec le nécessaire pour pouvoir supprimer des vhosts NGINX
- testez que vous pouvez facilement ajouter ou supprimer des Virtual Hosts depuis le fichier `host_vars` d'une machine donnée

# III. Affinons cette config

## 1. NGINX

➜ **On reste dans le *rôle* `nginx`**, faites en sorte que :

- on puisse déclarer la liste `vhosts` en *host_vars*
- si cette liste contient plusieurs `vhosts`, le *rôle* les déploie tous (exemple en dessous)
- le port précisé est automatiquement ouvert dans le firewall
- vous gérez explicitement les permissions de tous les fichiers
  - propriétaire, groupe propriétaire, et mode du fichier (permission `rwx`)

Exemple de fichier de variable avec plusieurs Virtual Hosts dans la liste `vhosts` :

```yml
vhosts:
  - site1:
    nginx_servername: site1.tp2.lab
    nginx_port: 8082
    nginx_webroot: /var/www/html/test2
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - site2:
    nginx_servername: site2.tp2.lab
    nginx_port: 8083
    nginx_webroot: /var/www/html/test3
    nginx_index_content: "<h1>teeeeeest 3</h1>"
```

➜ **Ajoutez une mécanique de `handlers/`**

- c'est un nouveau dossier à placer dans le *rôle*
- je vous laisse découvrir la mécanique par vous-mêmes et la mettre en place
- vous devez trigger un *handler* à chaque fois que la conf NGINX est modifiée
- vérifiez le bon fonctionnement
  - vous pouvez voir avec un `systemctl status` depuis quand une unité a été redémarrée

## 2. Common

➜ **On revient sur le rôle `common`**, les utilisateurs déployés doivent :

- avoir un password
- avoir un homedir
- avoir accès aux droits de `root` *via* `sudo`
- être dans un groupe `admin`
- avoir une clé SSH publique déposé dans leur `authorized_keys`

> Toutes ces données doivent être stockées dans les `group_vars`.

## 3. Dynamic loadbalancer

> Lisez la partie en entier avant de vous y attaquer :)

➜  **Créez un nouveau *rôle* : `apache`**

- ce rôle déploie un serveur web Apache
  - en environnement RedHat c'est `httpd` le paquet Apache
  - et `/etc/httpd/` pour la conf
  - idem pour le service
  - n'oubliez pas d'ouvrir un port firewall
- vous pouvez utiliser la commande `ansible-galaxy` pour générer un nouveau rôle avec les dossiers qu'on trouve usuellement dans un *rôle*

```bash
# Crée un dossier apache dans le dossier courant
$ ansible-galaxy role init apache
```

➜  **Créez un nouveau *rôle* : `webapp`**

- il repose sur le rôle `apache`
  - on verra plus tard comment déclarer des dépendances entre *roles*
  - on se contentera de les déclarer dans l'ordre dans le *playbook* pour le moment
- le vhost par défaut de Apache fera très bien l'affaire pour nous
- remplacez simplement le fichier `index.html` par défaut
  - par un fichier déposé avec le *module* `template`
  - un bête fichier HTML qui contient le nom de domaine de la machine (variable native Ansible)
  - cela nous permettra de vérifier sur quelle machine on atterit une fois qu'on aura notre loadbalancer :)

➜  **Créez un nouveau *rôle* : `rproxy` (pour *reverse proxy*)**

- déploie NGINX
  - paquet
  - service
    - ajout d'un *handler* en cas de modification de la conf
  - port firewall
- ajoute un fichier de conf
  - avec un `template`
  - ce fichier permet d'agir comme reverse proxy vers nos machines qui portent le *rôle* `webapp`
  - il écoute sur le port 443 pour proposer du HTTPS aux clients
  - vous trouverez un exemple pour ce fichier de conf juste en dessous
  - la section `upstream` contient la liste des serveurs web vers lesquels sont envoyés les clients

Exemple de configuration NGINX qui agit comme un reverse proxy vers deux machines :

```
upstream backend_hosts {
    server node1.tp2.lab;
    server node2.tp2.lab;
}

server {
    listen       80;
    server_name  webapp.tp2.lab;

    location / {
        proxy_pass   http://backend;
        proxy_set_header    Host $host;
    }
}
```

➜  **Adaptez le *rôle* : `common`**

- il doit désormais, en plus, templater le fichier `/etc/hosts` de toutes les machines

> Dans la conf au dessus, NGINX s'attend à recevoir des requêtes des clients sous la forme `http://webapp.tp2.lab`. Il serait donc utile de faire pointer le nom `webapp.tp2.lab` vers l'IP du proxy `10.2.1.13` dans le fichier `hosts` de votre machine pour faire les tests depuis votre navigateur/shell.

➜ **Effectuez le déploiement suivant :**

- ajoutez une troisième machine à votre déploiement Vagrant
  - `proxy.tp2.lab`
  - `10.2.1.13`
  - regénérez votre fichier `.ssh-config`
  - adaptez votre fichier `hosts`
- modifiez votre fichier d'inventaire Ansible `hosts.ini`
  - créez une catégorie `[web]` avec vos deux serveurs web `node1.tp2.lab` et `node2.tp2.lab`
  - créez une catégorie `[proxy]` avec le proxy `proxy.tp2.lab`
- adaptez votre *playbook*
  - déploie le *rôle* `common` sur tous les noeuds
  - les *rôles* `webapp` et `apache` sur le groupe `web`
  - le *rôle* `rproxy` sur le groupe `proxy`
- toute la magie réside dans le templating du fichier de conf NGINX :
  - il faut itérer avec une boucle sur les hôtes du groupe `web`
  - il est possible d'interroger les *facts* Ansible des autres machines depuis le template

> Pour rappel, les *facts* sont des données récoltées par Ansible avant le lancement d'une commande `ansible-playbook`. Ainsi il est possible par exemple d'utiliser l'IP d'un hôte pour la templater dans un fichier, cette adresse IP ayant été récoltée sous la forme d'un *fact* au tout début de l'exécution de la commande. C'est juste plein de variables pré-remplies et à votre disposition !

➜ **Vous pouvez interroger les *facts* depuis la ligne de commande**

```bash
# Récupérer tous les facts d'un groupe d'hôtes précis
$ ansible -i hosts.ini proxy -m setup

# Filtrer une partie des facts
$ ansible -i hosts.ini proxy -m setup -a "filter=ansible_eth1"
```

➜ **On obtient un configuration de loadbalancer dynamique**, et plus aucune connexion manuelle n'est nécessaire pour ajuster la taille du parc de serveurs web. Cela reste un exemple dans des conditions simplistes bien sûr.
